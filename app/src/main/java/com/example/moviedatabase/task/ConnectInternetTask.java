package com.example.moviedatabase.task;

import android.os.AsyncTask;
import android.util.Log;

import com.example.moviedatabase.interfaces.IConnectInternet;
import com.example.moviedatabase.model.FilmModel;
import com.example.moviedatabase.utilities.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.net.URL;

public class ConnectInternetTask extends AsyncTask<URL, Integer, FilmModel> {

    private IConnectInternet callBackHandler = null;
    public ConnectInternetTask(IConnectInternet callback) {
        this.callBackHandler = callback;
    }

    @Override
    protected FilmModel doInBackground(URL... urls) {
        String tmpResult = "";
        try {
            tmpResult = NetworkUtils.getResponseFromHttpUrl(urls[0]);
        } catch (IOException e) {
            Log.e("TAG", e.getMessage());
        }
        Log.d("TEST", tmpResult);

        Gson tmpGSON = new GsonBuilder().create();
        FilmModel tmpModel = tmpGSON.fromJson(tmpResult, FilmModel.class);

        Log.d("TAG", "RESULT ASLI = "+tmpModel.getTotalPages());

        return tmpModel;
    }

    @Override
    protected void onPostExecute(FilmModel s) {
        super.onPostExecute(s);
        Log.d("CHYNTIA",callBackHandler.toString());
        if(callBackHandler != null){
            callBackHandler.callback(s);
        }
    }
}

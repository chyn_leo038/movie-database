package com.example.moviedatabase.task;

import android.os.AsyncTask;
import android.util.Log;

import com.example.moviedatabase.interfaces.IFilmTrailer;
import com.example.moviedatabase.model.TrailerModel;
import com.example.moviedatabase.utilities.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.net.URL;

public class TrailerTask extends AsyncTask<URL, Integer, TrailerModel> {
    private IFilmTrailer callBackHandler = null;
    public TrailerTask(IFilmTrailer callback){
        this.callBackHandler = callback;
    }

    @Override
    protected TrailerModel doInBackground(URL... urls) {
        String tmpResult = "";
        try {
            tmpResult = NetworkUtils.getResponseFromHttpUrl(urls[0]);
        } catch (IOException e) {
            Log.e("TAG", e.getMessage());
        }
        Log.d("TEST", tmpResult);

        Gson tmpGSON = new GsonBuilder().create();
        TrailerModel tmpModel = tmpGSON.fromJson(tmpResult, TrailerModel.class);

        return tmpModel;
    }

    @Override
    protected void onPostExecute(TrailerModel s) {
        super.onPostExecute(s);
        Log.d("CHYNTIA",callBackHandler.toString());
        if(callBackHandler != null){
            callBackHandler.callback(s);
        }
    }
}

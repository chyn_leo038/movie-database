package com.example.moviedatabase.task;

import android.os.AsyncTask;
import android.util.Log;

import com.example.moviedatabase.interfaces.IFilmReviewDetail;
import com.example.moviedatabase.model.FilmReview;
import com.example.moviedatabase.utilities.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.net.URL;

public class FilmReviewTask extends AsyncTask<URL, Integer, FilmReview> {

    private IFilmReviewDetail callBackHandler = null;
    public FilmReviewTask(IFilmReviewDetail callback) {
        this.callBackHandler = callback;
    }

    @Override
    protected FilmReview doInBackground(URL... urls) {
        String tmpResult = "";
        try {
            tmpResult = NetworkUtils.getResponseFromHttpUrl(urls[0]);
        } catch (IOException e) {
            Log.e("TAG", e.getMessage());
        }
        Log.d("TEST", tmpResult);

        Gson tmpGSON = new GsonBuilder().create();
        FilmReview tmpModel = tmpGSON.fromJson(tmpResult, FilmReview.class);

        Log.d("TAG", "RESULT ASLI = "+tmpModel.getTotalPages());

        return tmpModel;
    }

    @Override
    protected void onPostExecute(FilmReview s) {
        super.onPostExecute(s);
        Log.d("CHYNTIA",callBackHandler.toString());
        if(callBackHandler != null){
            callBackHandler.callback(s);
        }
    }
}
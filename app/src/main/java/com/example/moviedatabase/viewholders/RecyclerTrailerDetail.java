package com.example.moviedatabase.viewholders;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviedatabase.R;
import com.example.moviedatabase.adapters.TrailerAdapter;
import com.example.moviedatabase.model.TrailerDetailModel;

public class RecyclerTrailerDetail extends RecyclerView.ViewHolder {

    private TextView tvTrailer;

    public RecyclerTrailerDetail(@NonNull View itemView) {
        super(itemView);
        tvTrailer = itemView.findViewById(R.id.tv_trailer);
    }

    public void bindData(String data, final TrailerAdapter.ListTrailerClickListener<TrailerDetailModel> mOnClickListener, final TrailerDetailModel trailer){
        Log.d("BINDRECYCLER","masuk sini kok");
        tvTrailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnClickListener.onListItemClicked(trailer);
            }
        });
        tvTrailer.setText(data);
    }
}

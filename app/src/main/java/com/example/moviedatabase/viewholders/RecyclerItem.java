package com.example.moviedatabase.viewholders;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviedatabase.R;
import com.example.moviedatabase.adapters.ItemAdapter;
import com.example.moviedatabase.model.FilmModel;
import com.example.moviedatabase.model.FilmSpec;
import com.squareup.picasso.Picasso;

public class RecyclerItem extends RecyclerView.ViewHolder {

    private ImageView iV;
    private TextView tvTitle;

    public RecyclerItem(@NonNull View itemView) {
        super(itemView);
        iV = itemView.findViewById(R.id.iv_image);
        tvTitle = itemView.findViewById(R.id.tv_title);
    }

    public void bindData(String data, final ItemAdapter.ListItemClickListener<FilmModel> mOnClickListener, final FilmSpec fSpec){
        Picasso.get().load(data).into(iV);
        iV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnClickListener.onListItemClicked(fSpec);
            }
        });
        tvTitle.setText(fSpec.getOriginalTitle());
    }
}

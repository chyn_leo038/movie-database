package com.example.moviedatabase.utilities;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * These utilities will be used to communicate with the network.
 */
public class NetworkUtils {

    final static String BASE_URL = "https://api.themoviedb.org/3/movie/";

    final static String KEY_CHYNTIA = "?api_key=00200b0d17b0c61c7dc58d760cb75902";

    /*
     * The sort field. One of stars, forks, or updated.
     * Default: results are sorted by best match if no field is specified.
     */
    /*
    final static String sortByTop = "top_rated";
    final static String sortByPopular = "popular";
    */

    public static URL buildUrl(String sortBY) throws MalformedURLException {
        String url =  BASE_URL+sortBY+KEY_CHYNTIA;
        Log.d("URL CHYNTIA",url);
        return new URL(url);
    }

    public static URL buildUrlFilmReview(int id) throws MalformedURLException {
        String url =  "https://api.themoviedb.org/3/movie/"+id+"/reviews?api_key=00200b0d17b0c61c7dc58d760cb75902&language=en-US&page=1";
        Log.d("URL CHYNTIA",url);
        return new URL(url);
    }

    public static URL buildUrlFilmTrailer(int id) throws MalformedURLException {
        String url =  "https://api.themoviedb.org/3/movie/"+id+"/videos?api_key=00200b0d17b0c61c7dc58d760cb75902&language=en-US";
        Log.d("URL CHYNTIA",url);
        return new URL(url);
    }

    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }
}
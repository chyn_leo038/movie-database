package com.example.moviedatabase;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviedatabase.adapters.TrailerAdapter;
import com.example.moviedatabase.favoritedatabase.FavoriteDatabase;
import com.example.moviedatabase.favoritedatabase.FavoriteEntry;
import com.example.moviedatabase.interfaces.IFilmReviewDetail;
import com.example.moviedatabase.interfaces.IFilmTrailer;
import com.example.moviedatabase.model.FilmReview;
import com.example.moviedatabase.model.FilmReviewDetail;
import com.example.moviedatabase.model.FilmSpec;
import com.example.moviedatabase.model.TrailerDetailModel;
import com.example.moviedatabase.model.TrailerModel;
import com.example.moviedatabase.task.FilmReviewTask;
import com.example.moviedatabase.task.TrailerTask;
import com.example.moviedatabase.utilities.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class FilmSpecActivity extends AppCompatActivity implements IFilmReviewDetail, IFilmTrailer, TrailerAdapter.ListTrailerClickListener {

    private RecyclerView rv_layout_trailers;
    TrailerAdapter tmpAdapterTrailer;

    private TextView tvTitle;
    private ImageView ivFavorite;
    private ImageView ivPoster;
    private TextView tvRelease;
    private TextView tvRating;
    private TextView tvSinopsis;
    private TextView tvDetailReviews;

    private FavoriteDatabase mDb;

    private FavoriteEntry favoriteEntry;

    private int DEFAULT_IMAGE_ID = R.drawable.loveicon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_spec);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        ivPoster = (ImageView) findViewById(R.id.iv_image);
        tvRelease = (TextView) findViewById(R.id.tv_release);
        tvRating = (TextView) findViewById(R.id.tv_rating);
        tvSinopsis = (TextView) findViewById(R.id.tv_sinopsis);
        ivFavorite = (ImageView) findViewById(R.id.btn_favorite);
        tvDetailReviews = (TextView) findViewById(R.id.tv_reviewdetail);

        mDb = FavoriteDatabase.getInstance(getApplicationContext());

        Intent intent = getIntent();
        if (intent.hasExtra("keyText")){
            final FilmSpec fSpec = (FilmSpec)intent.getParcelableExtra("keyText");
            Log.d("CHYNTIAMOVIEID", fSpec.getId()+"");
            tvTitle.setText(fSpec.getOriginalTitle());
            String BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w185";
            Picasso.get().load(BASE_URL_IMAGE+fSpec.getPosterPath()).into(ivPoster);
            tvRelease.setText(fSpec.getReleaseDate());
            tvRating.setText(fSpec.getVoteAverage().toString() + " / 10");
            tvSinopsis.setText(fSpec.getOverview());
            FavoriteExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    favoriteEntry = mDb.favoriteDao().loadFavoriteById(fSpec.getId());
                    if(favoriteEntry == null){
                        ivFavorite.setImageResource(R.drawable.loveicon);
                    } else {
                        ivFavorite.setImageResource(R.drawable.lovefavorite);
                        DEFAULT_IMAGE_ID = R.drawable.lovefavorite;
                    }
                }
            });
            ivFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(DEFAULT_IMAGE_ID == R.drawable.lovefavorite){
                        ivFavorite.setImageResource(R.drawable.loveicon);
                        DEFAULT_IMAGE_ID = R.drawable.loveicon;

//                        Log.d("CHYNTIA INSERT", fSpec.getId().toString());
                        FavoriteExecutors.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                FavoriteEntry favoriteEntry = new FavoriteEntry(fSpec.getId(), fSpec.getOriginalTitle(), fSpec.getPosterPath(),fSpec.getOverview(),fSpec.getVoteAverage(),fSpec.getReleaseDate());
                                mDb.favoriteDao().deleteFavorite(favoriteEntry.getId());
                            }
                        });
                    } else if(DEFAULT_IMAGE_ID == R.drawable.loveicon){
                        ivFavorite.setImageResource(R.drawable.lovefavorite);
                        DEFAULT_IMAGE_ID = R.drawable.lovefavorite;

                        FavoriteExecutors.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                FavoriteEntry favoriteEntry = new FavoriteEntry(fSpec.getId(), fSpec.getOriginalTitle(), fSpec.getPosterPath(),fSpec.getOverview(),fSpec.getVoteAverage(),fSpec.getReleaseDate());
                                mDb.favoriteDao().insertFavorite(favoriteEntry);

                            }
                        });
                    }
                }
            });


            // Buat trailers
            tmpAdapterTrailer = new TrailerAdapter(this);
            rv_layout_trailers = findViewById(R.id.layout_detail_trailers_recycler);
            rv_layout_trailers.setHasFixedSize(true);
            rv_layout_trailers.setLayoutManager(new LinearLayoutManager(this));


            URL url = null;
            // 1. Build URL
            try {
                url = NetworkUtils.buildUrlFilmTrailer(fSpec.getId());
            } catch (MalformedURLException e) {
                String textToShow = "URL gagal build";
            }

            if(url != null){
                // 3. Do HTTPRequest
                new TrailerTask(this).execute(url);

            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tmpAdapterTrailer.notifyDataSetChanged();
                }
            });
            rv_layout_trailers.setAdapter(tmpAdapterTrailer);


            // Buat Reviews
            URL url2 = null;
            // 1. Build URL
            try {
                url2 = NetworkUtils.buildUrlFilmReview(fSpec.getId());
            } catch (MalformedURLException e) {
                String textToShow = "URL gagal build";
            }

            if(url2 != null){
                // 3. Do HTTPRequest
                new FilmReviewTask(this).execute(url2);

            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            NavUtils.navigateUpFromSameTask(this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void callback(FilmReview obj) {
        Log.d("SPECIFICATION",obj.getFilmReviewDetails().size()+"");
        StringBuffer sB = new StringBuffer();
        for (FilmReviewDetail frDetail : obj.getFilmReviewDetails()){
            sB.append(frDetail.getContent());
            sB.append("\n\n -Reviewed by "+frDetail.getAuthor());
            sB.append("\n\n----------------\n\n");
        }
        tvDetailReviews.setText(sB);
    }

    @Override
    public void callback(TrailerModel obj) {
        Log.d("SPECIFICATION2",obj.getTrailerDetailModelList().size()+"");
        if(obj != null){
            Log.d("SPECIFICATION3",obj.getTrailerDetailModelList().size()+"");
            tmpAdapterTrailer = new TrailerAdapter(this);
            tmpAdapterTrailer.setDataArray((ArrayList<TrailerDetailModel>) obj.getTrailerDetailModelList());
            rv_layout_trailers.setAdapter(tmpAdapterTrailer);
        }
    }

    @Override
    public void onListItemClicked(TrailerDetailModel t) {
        Log.d("LinkYoutube",t.getName()+"");
        String url = "http://youtube.com/watch?v="+t.getKey();
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if(intent.resolveActivity(getPackageManager())!=null){
            startActivity(intent);
        }
    }
}

package com.example.moviedatabase.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrailerModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("results")
    @Expose
    private List<TrailerDetailModel> trailerDetailModelList = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<TrailerDetailModel> getTrailerDetailModelList() {
        return trailerDetailModelList;
    }

    public void setTrailerDetailModelList(List<TrailerDetailModel> trailerDetailModelList) {
        this.trailerDetailModelList = trailerDetailModelList;
    }
}

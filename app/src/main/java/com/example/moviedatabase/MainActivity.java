package com.example.moviedatabase;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviedatabase.adapters.ItemAdapter;
import com.example.moviedatabase.favoritedatabase.FavoriteDatabase;
import com.example.moviedatabase.favoritedatabase.FavoriteEntry;
import com.example.moviedatabase.interfaces.IConnectInternet;
import com.example.moviedatabase.model.FavoriteViewModel;
import com.example.moviedatabase.model.FilmModel;
import com.example.moviedatabase.model.FilmSpec;
import com.example.moviedatabase.task.ConnectInternetTask;
import com.example.moviedatabase.utilities.NetworkUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IConnectInternet, ItemAdapter.ListItemClickListener {

    private RecyclerView rv_layout;
    private ImageView mFilm;
    ItemAdapter tmpAdapter = new ItemAdapter(this);

    FrameLayout mLayoutProgress;

    String sortBy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init(savedInstanceState);

        rv_layout.setAdapter(tmpAdapter);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("VIEW_STATE_KEY", sortBy);
    }

    public void init(Bundle savedInstanceState){
        mLayoutProgress = findViewById(R.id.layout_Progress);
        mLayoutProgress.setVisibility(View.VISIBLE);

        rv_layout = findViewById(R.id.layout_mainRecycler);
        rv_layout.setHasFixedSize(true);
        rv_layout.setLayoutManager(new GridLayoutManager(this,2){
            @Override
            public void onLayoutCompleted(RecyclerView.State state) {
                super.onLayoutCompleted(state);
                mLayoutProgress.setVisibility(View.INVISIBLE);
            }
        });

        mFilm = findViewById(R.id.iv_image);

        if(savedInstanceState != null){
            sortBy = savedInstanceState.getString("VIEW_STATE_KEY");
        } else {
            sortBy = "popular";
        }
        if(sortBy.equals("favorites")){
            setupViewModel();
        } else {
            getMovieFromAPI(sortBy);
        }
    }

    @Override
    public void callback(FilmModel obj) {
        if(obj != null){
            tmpAdapter = new ItemAdapter(this);
            tmpAdapter.setDataArray((ArrayList<FilmSpec>) obj.getResults());
            rv_layout.setAdapter(tmpAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemThatWasClickedId = item.getItemId();
        if(itemThatWasClickedId == R.id.action_popular){
            sortBy = "popular";
            getMovieFromAPI(sortBy);
        } else if(itemThatWasClickedId == R.id.action_top_rated){
            sortBy = "top_rated";
            getMovieFromAPI(sortBy);
        } else if(itemThatWasClickedId == R.id.action_favorite){
            sortBy = "favorites";
            setupViewModel();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupViewModel() {
        FavoriteViewModel viewModel = ViewModelProviders.of(this).get(FavoriteViewModel.class);
        viewModel.getFavorites().observe(this, new Observer<List<FavoriteEntry>>() {
            @Override
            public void onChanged(List<FavoriteEntry> favoriteEntries) {
                ArrayList<FilmSpec> aListFSpec= new ArrayList<>();
                for (FavoriteEntry favoriteEntry : favoriteEntries){
                    aListFSpec.add(new FilmSpec(favoriteEntry.getId(), favoriteEntry.getTitle(),favoriteEntry.getPosterPath(),favoriteEntry.getReleaseDate(),favoriteEntry.getVoteAverage(),favoriteEntry.getOverview()));
                }
                Log.d("CHYNTIA",aListFSpec.toString());
                tmpAdapter = new ItemAdapter(MainActivity.this);
                tmpAdapter.setDataArray(aListFSpec);
                rv_layout.setAdapter(tmpAdapter);
            }
        });

    }
    public void getMovieFromAPI(String sortBy){
        mLayoutProgress.setVisibility(View.VISIBLE);
        URL url = null;
        // 1. Build URL
        try {
            url = NetworkUtils.buildUrl(sortBy);
        } catch (MalformedURLException e) {
            //If build url failed, what should I do?
            // mau bikin toast?
            String textToShow = "URL gagal build";
        }

        if(url != null){
            // 3. Do HTTPRequest
            new ConnectInternetTask(this).execute(url);

        }
        rv_layout.setAdapter(tmpAdapter);

    }

    @Override
    public void onListItemClicked(FilmSpec fSpec) {
        Intent intent = new Intent(this, FilmSpecActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("keyText",fSpec);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}

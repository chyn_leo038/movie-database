package com.example.moviedatabase.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviedatabase.R;
import com.example.moviedatabase.model.TrailerDetailModel;
import com.example.moviedatabase.viewholders.RecyclerTrailerDetail;

import java.util.ArrayList;

public class TrailerAdapter extends RecyclerView.Adapter {

    private final ListTrailerClickListener<TrailerDetailModel> mOnClickListener;

    public interface ListTrailerClickListener<T>{
        void onListItemClicked(TrailerDetailModel t);
    }

    public TrailerAdapter(ListTrailerClickListener<TrailerDetailModel> mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    private ArrayList<TrailerDetailModel> arrData = new ArrayList<>();

    public void setDataArray(ArrayList<TrailerDetailModel> data){
        Log.d("DataArray",data.size()+"");
        this.arrData = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.trailers_detail_recycler, parent, false);
        RecyclerTrailerDetail vh = new RecyclerTrailerDetail(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((RecyclerTrailerDetail) holder).bindData(arrData.get(position).getName(), mOnClickListener, arrData.get(position));
    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }
}

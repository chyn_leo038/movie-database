package com.example.moviedatabase.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviedatabase.R;
import com.example.moviedatabase.model.FilmModel;
import com.example.moviedatabase.model.FilmSpec;
import com.example.moviedatabase.viewholders.RecyclerItem;

import java.util.ArrayList;

public class ItemAdapter extends RecyclerView.Adapter {

    private final ListItemClickListener<FilmModel> mOnClickListener;

    public interface ListItemClickListener<T>{
        void onListItemClicked(FilmSpec t);
    }

    public ItemAdapter(ListItemClickListener<FilmModel> mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }


    private ArrayList<FilmSpec> arrData = new ArrayList<>();

    public void setDataArray(ArrayList<FilmSpec> data){
        for (FilmSpec fSpec : data) {
            Log.d("CHYNTIAADAPTER",fSpec.getOriginalTitle());
        }

        this.arrData = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler, parent, false);
        RecyclerItem vh = new RecyclerItem(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        // bind data
        String BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w185";
        ((RecyclerItem) holder).bindData(BASE_URL_IMAGE+arrData.get(position).getPosterPath(), mOnClickListener, arrData.get(position));

    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }
}

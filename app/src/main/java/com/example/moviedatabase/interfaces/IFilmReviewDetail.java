package com.example.moviedatabase.interfaces;

import com.example.moviedatabase.model.FilmReview;

public interface IFilmReviewDetail {
    void callback(FilmReview obj);
}

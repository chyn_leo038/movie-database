package com.example.moviedatabase.interfaces;

import com.example.moviedatabase.model.FilmModel;

public interface IConnectInternet {
    void callback(FilmModel obj);
}
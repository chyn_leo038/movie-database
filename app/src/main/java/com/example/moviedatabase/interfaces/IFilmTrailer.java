package com.example.moviedatabase.interfaces;

import com.example.moviedatabase.model.TrailerModel;

public interface IFilmTrailer {
    void callback(TrailerModel obj);
}
